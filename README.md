# small-orm-forms
Small Orm is a small php ORM.

This package provide classes for form validation.

## Install

Require Small ORM Core package (https://framagit.org/small/small-orm-core) :
```
composer require small/small-orm-core
```

Require the package with composer:
```
composer require small/small-orm-forms
```

## Documentation

You can find the documentation here : [documentation](https://framagit.org/small/small-orm-doc)
