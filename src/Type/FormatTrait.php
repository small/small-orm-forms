<?php

/**
 * This file is a part of small-orm-core
 * Copyright 2021-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace Sebk\SmallOrmForms\Type;

trait FormatTrait
{
    /**
     * @var mixed
     */
    protected $format;

    /**
     * Get format
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set format
     * @param mixed $format
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }
}