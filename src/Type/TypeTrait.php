<?php

/**
 * This file is a part of small-orm-core
 * Copyright 2021-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace Sebk\SmallOrmForms\Type;

trait TypeTrait
{

    /** @var string */
    protected $type;

    /**
     * Get type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     * @param $type
     * @return $this
     * @throws TypeNotFoundException
     */
    public function setType($type)
    {
        if (!class_exists(__NAMESPACE__ . '\\' . ucfirst($type) . 'Type')) {
            throw new TypeNotFoundException("Invalid type ($type)");
        }

        $this->type = $type;

        return $this;
    }

}
