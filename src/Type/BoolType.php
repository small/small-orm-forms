<?php

/**
 * This file is a part of small-orm-core
 * Copyright 2021-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace Sebk\SmallOrmForms\Type;

class BoolType implements TypeInterface
{
    const TYPE_BOOL = "bool";

    use TypeTrait;

    public function __contruct()
    {
        $this->setType(self::TYPE_BOOL);
    }

    /**
     * Validate a value
     * @param $value
     * @return bool
     */
    public function validate($value)
    {
        if (!is_bool($value) && $value != null) {
            return false;
        }

        return true;
    }

    /**
     * Reformat a value
     * @param $value
     * @return int
     */
    public function reformat($value)
    {
        if ($value === null) {
            return null;
        }
        return (bool)$value;
    }
}
